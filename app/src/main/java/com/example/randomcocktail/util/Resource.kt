package com.example.randomcocktail.util

import com.example.randomcocktail.model.Drinks

sealed class Resource {
    data class Success(val data: Drinks): Resource()
    data class Error(val err: String): Resource()
    object Loading: Resource()
    object Standby: Resource()
}