package com.example.randomcocktail.model

import com.example.randomcocktail.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

object CocktailRepo {
    private val cocktailDbApi by lazy { CocktailApi.cocktailDbApi }

    suspend fun fetchRandomDrink(): Resource = withContext(Dispatchers.IO) {
        return@withContext try {
            val res = cocktailDbApi.getRandom()
            if(res.isSuccessful) {
                Resource.Success(res.body()!!)
            } else {
                Resource.Error("REPO ERROR")
            }
        } catch (e: Exception) {
            Resource.Error(e.toString())
        }
    }

    suspend fun fetchDrinkByID(id: Int): Resource = withContext(Dispatchers.IO) {
        return@withContext try {
            val res = cocktailDbApi.getByID(id)
            if(res.isSuccessful) {
                Resource.Success(res.body()!!)
            } else {
                Resource.Error("REPO ERROR")
            }
        } catch (e: Exception) {
            Resource.Error(e.toString())
        }
    }
}