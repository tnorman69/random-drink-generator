package com.example.randomcocktail.model

import com.example.randomcocktail.view.FlipperProvider
import okhttp3.OkHttp
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface CocktailApi {
    @GET("/api/json/v1/1/random.php")
    suspend fun getRandom(): Response<Drinks>

    @GET("/api/json/v1/1/lookup.php")
    suspend fun getByID(@Query("i")i: Int = 11007): Response<Drinks>

    companion object {

        private val okhttp by lazy {
            OkHttpClient.Builder().addNetworkInterceptor(FlipperProvider.getFlipperOkhttpInterceptor()).build()
        }

        val cocktailDbApi: CocktailApi by lazy {
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://www.thecocktaildb.com")
                .client(okhttp)
                .build()
                .create(CocktailApi::class.java)
        }
    }

}