package com.example.randomcocktail.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.randomcocktail.databinding.AlcoholCardBinding
import com.example.randomcocktail.loadImage
import com.example.randomcocktail.model.Drink

class DrinksListAdapter: RecyclerView.Adapter<DrinksListAdapter.DrinksViewHolder>() {
    private val drinksList = mutableListOf<Drink>()

    class DrinksViewHolder(private val binding: AlcoholCardBinding): RecyclerView.ViewHolder(binding.root) {
        fun addDrink(drink: Drink) {
            binding.ivDetail.loadImage(drink.strDrinkThumb)
            binding.tvName.text = drink.strDrink
            binding.root.setOnClickListener {
                val action = DrinksRandomListFragmentDirections.actionDrinksRandomListFragmentToDrinksDetailFragment(drink.idDrink.toInt())
                binding.root.findNavController().navigate(action)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinksViewHolder {
        return DrinksViewHolder(AlcoholCardBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: DrinksViewHolder, position: Int) {
        holder.addDrink(drinksList[position])
    }

    override fun getItemCount(): Int {
        return drinksList.size
    }

    fun clearData() {
        drinksList.clear()
    }

    fun addData(data: List<Drink>) {
        drinksList.addAll(data)
    }
}