package com.example.randomcocktail.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.randomcocktail.databinding.DrinksRandomFragmentBinding
import com.example.randomcocktail.util.Resource
import com.example.randomcocktail.viewmodel.BlankViewModelFactory
import com.example.randomcocktail.viewmodel.DrinksViewModel

class DrinksRandomListFragment: Fragment() {

    private val TAG = "LIST_FRAGMENT"

    private var _binding: DrinksRandomFragmentBinding? = null
    private val binding: DrinksRandomFragmentBinding get() = _binding!!
    private val drinksViewModel: DrinksViewModel by lazy {
        ViewModelProvider(this, BlankViewModelFactory())[DrinksViewModel::class.java]
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = DrinksRandomFragmentBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinksViewModel.fetchRandomDrinks()
        initViews()
    }

    private fun initViews() {

        val adapter = DrinksListAdapter()

        drinksViewModel.state.observe(viewLifecycleOwner) {
            when(it) {
                is Resource.Error -> {
                    Log.e(TAG, it.err)
                    drinksViewModel.setStandby()
                }
                Resource.Loading -> {
                    disableButtons()
                }
                Resource.Standby -> {
                    enableButtons()
                }
                is Resource.Success -> {
                    binding.recycler.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL,false)
                    adapter.addData(it.data.drinks)
                    binding.recycler.adapter = adapter
                    drinksViewModel.setStandby()
                }
            }
        }

        binding.btnRandom.setOnClickListener {
            adapter.clearData()
            drinksViewModel.fetchRandomDrinks()
        }
    }

    private fun enableButtons() = with(binding) {
        progress.isVisible = false
        btnRandom.isEnabled = true
    }

    private fun disableButtons() = with(binding) {
        progress.isVisible = true
        btnRandom.isEnabled = false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
