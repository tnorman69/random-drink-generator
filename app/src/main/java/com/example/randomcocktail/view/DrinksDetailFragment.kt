package com.example.randomcocktail.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.example.randomcocktail.R
import com.example.randomcocktail.databinding.DrinksDetailFragmentBinding
import com.example.randomcocktail.loadImage
import com.example.randomcocktail.model.Drink
import com.example.randomcocktail.util.Resource
import com.example.randomcocktail.viewmodel.BlankViewModelFactory
import com.example.randomcocktail.viewmodel.DrinksViewModel

class DrinksDetailFragment: Fragment() {
    private var _binding: DrinksDetailFragmentBinding? = null
    private val binding: DrinksDetailFragmentBinding get() = _binding!!
    private val drinksViewModel: DrinksViewModel by lazy {
        ViewModelProvider(this, BlankViewModelFactory())[DrinksViewModel::class.java]
    }
    private val TAG = "DETAIL_FRAGMENT"
    private val args by navArgs<DrinksDetailFragmentArgs>()

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = DrinksDetailFragmentBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        drinksViewModel.state.observe(viewLifecycleOwner) {
            when(it) {
                is Resource.Error -> {
                    Log.e(TAG, it.err)
                    drinksViewModel.setStandby()
                }
                is Resource.Loading -> {
                    disableButtons()
                }
                is Resource.Standby -> {
                    enableButtons()
                }
                is Resource.Success -> {
                    with(it.data.drinks[0]) {
                        ivDetail.loadImage(this.strDrinkThumb)
                        tvIngredients.setIngredients(this)
                        tvMeasures.setMeasures(this)
                        tvInstructions.text = this.strInstructions
                        tvNameId.text = strDrink
                    }
                    drinksViewModel.setStandby()
                }
            }
        }

        drinksViewModel.fetchDrinkByID(args.drinkId)
    }

    private fun enableButtons() = with(binding) {
        progress.isVisible = false
    }

    private fun disableButtons() = with(binding) {
        progress.isVisible = true
    }


    private fun TextView.setIngredients(drink: Drink) {
        var list = listOf(
            drink.strIngredient1,
            drink.strIngredient2,
            drink.strIngredient3,
            drink.strIngredient4,
            drink.strIngredient5,
            drink.strIngredient6,
            drink.strIngredient7,
            drink.strIngredient8,
            drink.strIngredient9,
            drink.strIngredient10,
            drink.strIngredient11,
            drink.strIngredient12,
            drink.strIngredient13,
            drink.strIngredient14,
            drink.strIngredient15)
        list = list.filterNotNull()
        this.text = getString(R.string.tv_ingredients,
            list.joinToString(separator = "\n", prefix = "\n"))
    }
    private fun TextView.setMeasures(drink: Drink) {
        var list = listOf(
            drink.strMeasure1,
            drink.strMeasure2,
            drink.strMeasure3,
            drink.strMeasure4,
            drink.strMeasure5,
            drink.strMeasure6,
            drink.strMeasure7,
            drink.strMeasure8,
            drink.strMeasure9,
            drink.strMeasure10,
            drink.strMeasure11,
            drink.strMeasure12,
            drink.strMeasure13,
            drink.strMeasure14,
            drink.strMeasure15)
        list = list.filterNotNull()
        this.text = getString(R.string.tv_measures,
            list.joinToString(separator = "\n", prefix = "\n"))
    }
}

