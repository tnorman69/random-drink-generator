package com.example.randomcocktail

import android.app.Application
import com.example.randomcocktail.view.FlipperProvider

class BaseApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        FlipperProvider.init(this)
    }
}