package com.example.randomcocktail.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.randomcocktail.model.CocktailRepo
import com.example.randomcocktail.model.Drink
import com.example.randomcocktail.model.Drinks
import com.example.randomcocktail.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DrinksViewModel: ViewModel() {
    private var _state: MutableLiveData<Resource> = MutableLiveData(Resource.Standby)
    val state: LiveData<Resource> get() = _state

    private val repo = CocktailRepo

    private fun setLoading() {
        _state.value = Resource.Loading
    }

    fun setStandby() {
        _state.value = Resource.Standby
    }

    fun fetchRandomDrinks(amount: Int = 5) = viewModelScope.launch(Dispatchers.Main) {
        setLoading()
        repeat(amount) {
            _state.value = repo.fetchRandomDrink()
        }
    }

    fun fetchDrinkByID(id: Int) = viewModelScope.launch(Dispatchers.Main) {
        setLoading()
        _state.value = repo.fetchDrinkByID(id)
    }
}